﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZxZOnlineExam.WebApi.Tools.Pagination
{
    public class Pagination
    {
        public int PageIndex { get; set; } = 1;

        private int _pageSize;
        public int PageSize { 
            get {
                return _pageSize;
            }
            set {
                if (value>50)
                {
                    _pageSize = 50;
                }
                _pageSize = value;
            } 
        }

        public int Total { get; set; }

        public object Data { get; set; }
    }
}
