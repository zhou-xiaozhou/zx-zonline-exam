﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZxZOnlineExam.Dtos.Question;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.WebApi.AutoMapperProfiles
{
    public class QuestionProfile : Profile
    {
        public QuestionProfile()
        {
            CreateMap<Question, QuestionDto>().
                ForMember(destinationMember => destinationMember.SubjectDto, memberOptions => memberOptions.MapFrom(sourceMember => sourceMember.Subject))
                .ForMember(destinationMember=>destinationMember.QuestionOptionDto,memberOptions=>memberOptions.MapFrom(sourceMember=>sourceMember.QuestionOption));
            CreateMap<QuestionDtoForCreation, Question>()
                .ForMember(destinationMember => destinationMember.Id, memberOptions => memberOptions.MapFrom(sourceMember => Guid.NewGuid()));
        }
    }
}
