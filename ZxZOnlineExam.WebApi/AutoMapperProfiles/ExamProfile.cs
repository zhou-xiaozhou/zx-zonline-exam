﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZxZOnlineExam.Dtos.Exam;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.WebApi.AutoMapperProfiles
{
    public class ExamProfile : Profile
    {
        public ExamProfile()
        {
            CreateMap<Exam, ExamDto>();
        }
    }
}
