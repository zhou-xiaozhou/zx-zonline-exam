﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZxZOnlineExam.Dtos.Question;
using ZxZOnlineExam.Dtos.QuestionOption;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.WebApi.AutoMapperProfiles
{
    public class QuestionOptionProfile : Profile
    {
        public QuestionOptionProfile()
        {
            CreateMap<QuestionOption, QuestionOptionDto>();
            CreateMap<QuestionDtoForCreation, QuestionOption>()
                .ForMember(destinationMember => destinationMember.Id, memberOptions => memberOptions.MapFrom(sourceMember => Guid.NewGuid()));
        }
    }
}
