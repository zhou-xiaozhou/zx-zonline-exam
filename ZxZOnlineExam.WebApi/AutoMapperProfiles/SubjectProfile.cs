﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZxZOnlineExam.Dtos.Subject;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.WebApi.AutoMapperProfiles
{
    public class SubjectProfile:Profile
    {
        public SubjectProfile()
        {
            CreateMap<Subject, SubjectDto>();
            CreateMap<SubjectDtoForCreation, Subject>().ForMember(destinationMember => destinationMember.Id, memberOptions => memberOptions.MapFrom(sourceMember => Guid.NewGuid()));
            CreateMap<SubjectDtoForUpdate, Subject>();
        }
    }
}
