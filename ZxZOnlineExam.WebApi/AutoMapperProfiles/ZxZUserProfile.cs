﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZxZOnlineExam.Dtos.ZxZUser;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.WebApi.AutoMapperProfiles
{
    public class ZxZUserProfile : Profile
    {
        public ZxZUserProfile()
        {
            CreateMap<ZxZUser, ZxZUserDto>();
        }
    }
}
