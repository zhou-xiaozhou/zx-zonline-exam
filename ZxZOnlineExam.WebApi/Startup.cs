using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Http;
using AutoMapper;
using ZxZOnlineExam.Models;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.Repository;
using ZxZOnlineExam.IService;
using ZxZOnlineExam.Service;

namespace ZxZOnlineExam.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //添加identity服务
            services.AddIdentity<ZxZUser, ZxZRole>(option=> {
                //修改identify中password约束
                option.Password.RequiredLength = 4;
                option.Password.RequiredUniqueChars = 0;
                option.Password.RequireNonAlphanumeric = false;
                option.Password.RequireUppercase = false;
                option.Password.RequireLowercase = false;
                option.Password.RequireDigit = false;
            }).AddEntityFrameworkStores<ZxZOnlineExamDbContext>();

            //添加JWT验证服务
            services.AddAuthentication(/*JwtBearerDefaults.AuthenticationScheme*/options => { 
                //认证middleware配置
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
            {
                var secretByte = Encoding.UTF8.GetBytes(Configuration["Authentication:Secretkey"]);
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["Authentication:Issuer"],

                    ValidateAudience = true,
                    ValidAudience = Configuration["Authentication:Audience"],

                    ValidateLifetime = true,

                    IssuerSigningKey = new SymmetricSecurityKey(secretByte)

                };
            });

            services.AddControllers(setupAction =>
            {
                setupAction.ReturnHttpNotAcceptable = true;
                //setupAction.OutputFormatters.Add(
                //    new XmlDataContractSerializerOutputFormatter());//支持XML
            })
            .AddNewtonsoftJson(setupAction =>
            {
                setupAction.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            })
            .AddXmlDataContractSerializerFormatters()//支持XML
            .ConfigureApiBehaviorOptions(setupAction =>//输出状态码422
            {
                setupAction.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetail = new ValidationProblemDetails(context.ModelState)
                    {
                        Type = "无所谓",
                        Title = "数据验证失败",
                        Status = StatusCodes.Status422UnprocessableEntity,
                        Detail = "请看详细说明",
                        Instance = context.HttpContext.Request.Path
                    };
                    problemDetail.Extensions.Add("traceId", context.HttpContext.TraceIdentifier);
                    return new UnprocessableEntityObjectResult(problemDetail)
                    {
                        ContentTypes = { "application/problem+json" }
                    };
                };
            });
            
            services.AddDbContext<ZxZOnlineExamDbContext>(option =>
            {
                //option.UseSqlServer("server=192.168.31.157,1433;Database=FakeXiechengDb;User Id=sa;Password=Sa123456");
                //option.UseSqlServer(Configuration["DbContext:MsSQLConnectionString"]);
                option.UseSqlServer(Configuration.GetConnectionString("MsSqlConnStr"));
                //option.UseMySql(Configuration["DbContext:MySQLConnectionString"]);
                //option.UseMySql(Configuration["DbContext:MySQLDockerConnectionString"]);
            });

            //扫描profile文件
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            //services.AddAutoMapper(typeof(TouristRouteProfile).Assembly, typeof(TouristRoutePictureProfile).Assembly);


            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo { Title = "FakeXiecheng.API", Version = "v1" });
            //});

            services.AddHttpClient();

            //services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();



            //services.Configure<MvcOptions>(config =>
            //{
            //    var outputFormatter = config.OutputFormatters.OfType<NewtonsoftJsonOutputFormatter>()?.FirstOrDefault();
            //    if (outputFormatter != null)
            //    {
            //        outputFormatter.SupportedMediaTypes.Add("application/vnd.zssk.hateoas+json");
            //    }
            //});


            //配置依赖注入服务
            UserIOCConfig(services);
        }

        public void UserIOCConfig(IServiceCollection services)
        {
            services.AddScoped<ISubjectRepository, SubjectRepository>();
            services.AddScoped<ISubjectService, SubjectService>();

            services.AddScoped<IQuestionRepository, QuestionRepository>();
            services.AddScoped<IQuestionService, QuestionService>(); 

            services.AddScoped<IQuestionOptionRespository, QuestionOptionRespository>();
            services.AddScoped<IQuestionOptionService, QuestionOptionService>();

            services.AddScoped<IExamRepository, ExamRepository>();
            services.AddScoped<IExamService, ExamService>();

            services.AddScoped<IExamToStudentRepository, ExamToStudentRepository>();
            services.AddScoped<IExamToStudentService, ExamToStudentService>();

            services.AddScoped<IExamToQuestionRepository, ExamToQuestionRepository>();
            services.AddScoped<IExamToQuestionService, ExamToQuestionService>();

            services.AddScoped<IAnswerSheetRepository, AnswerSheetRepository>();
            services.AddScoped<IAnswerSheetService, AnswerSheetService>();

            services.AddScoped<IScoreRepository, ScoreRepository>();
            services.AddScoped<IScoreService, ScoreService>();

            services.AddScoped<IWrongQuestionRecordRepository, WrongQuestionRecordRepository>();
            services.AddScoped<IWrongQuestionRecordService, WrongQuestionRecordService>();
        } 

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseRouting();

            
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
