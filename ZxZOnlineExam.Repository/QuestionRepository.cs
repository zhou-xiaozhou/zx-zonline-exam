﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Repository
{
    public class QuestionRepository : BaseEntityRepository<Question>, IQuestionRepository
    {
        private readonly ZxZOnlineExamDbContext _dbContext;

        public QuestionRepository(ZxZOnlineExamDbContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

        #region 重写Get方法，因为要在Dbconxt操作中IncludeQuestionOption
        public override async Task<Question> GetSingleAsync(Guid id)
        {
            var result = _dbContext.Questions.Where(t => t.IsRemoved == false).Include(q=>q.Subject).Include(q=>q.QuestionOption).AsQueryable();
            return await result.Where(t => t.Id == id).FirstOrDefaultAsync();
        }

        public override async Task<IEnumerable<Question>> GetAsync()
        {
            var result = _dbContext.Questions.Where(t => t.IsRemoved == false).Include(q => q.Subject).Include(q => q.QuestionOption).AsQueryable();
            return await result.ToListAsync();
        }

        public override async Task<IEnumerable<Question>> GetAsync(bool asc)
        {
            var result = _dbContext.Questions.Where(t => t.IsRemoved == false).Include(q => q.Subject).Include(q => q.QuestionOption).AsQueryable();
            if (asc)
            {
                result = result.OrderBy(r => r.CreateTime);
            }
            else
            {
                result = result.OrderByDescending(r => r.CreateTime);
            }
            return await result.ToListAsync();
        }

        public override async Task<IEnumerable<Question>> GetAsync(int pageIndex, int pageSize = 10)
        {
            var result = _dbContext.Questions.Where(t => t.IsRemoved == false).Include(q => q.Subject).Include(q => q.QuestionOption).AsQueryable();
            result = result.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return await result.ToListAsync();
        }

        public override async Task<IEnumerable<Question>> GetAsync(Expression<Func<Question, bool>> expression)
        {
            var result = _dbContext.Questions.Where(t => t.IsRemoved == false).Include(q => q.Subject).Include(q => q.QuestionOption).AsQueryable();
            result = result.Where(expression);
            return await result.ToListAsync();
        }

        public override async Task<IEnumerable<Question>> GetAsync(Expression<Func<Question, bool>> expression, bool asc)
        {
            var result = _dbContext.Questions.Where(t => t.IsRemoved == false).Include(q => q.Subject).Include(q => q.QuestionOption).AsQueryable();
            result = result.Where(expression);
            if (asc)
            {
                result = result.OrderBy(r => r.CreateTime);
            }
            else
            {
                result = result.OrderByDescending(r => r.CreateTime);
            }
            return await result.ToListAsync();
        }

        public override async Task<IEnumerable<Question>> GetAsync(Expression<Func<Question, bool>> expression, bool asc, int pageIndex, int pageSize = 10)
        {
            var result = _dbContext.Questions.Where(t => t.IsRemoved == false).Include(q => q.Subject).Include(q => q.QuestionOption).AsQueryable();
            result = result.Where(expression);
            if (asc)
            {
                result = result.OrderBy(r => r.CreateTime);
            }
            else
            {
                result = result.OrderByDescending(r => r.CreateTime);
            }
            result = result.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return await result.ToListAsync();
        }
        #endregion
    }
}
