﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Repository
{
    public class ExamToStudentRepository : BaseEntityRepository<ExamToStudent>, IExamToStudentRepository
    {
        private readonly ZxZOnlineExamDbContext _dbContext;

        public ExamToStudentRepository(ZxZOnlineExamDbContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

        public override async Task<ExamToStudent> GetSingleAsync(Guid id)
        {
            return await _dbContext.ExamToStudents.Include(e => e.Exam).Include(e => e.ZxZUser).Where(e => e.Id == id).FirstOrDefaultAsync();
        }
    }
}
