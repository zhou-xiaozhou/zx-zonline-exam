﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Repository
{
    public class ExamToQuestionRepository : BaseEntityRepository<ExamToQuestion>, IExamToQuestionRepository
    {
        private readonly ZxZOnlineExamDbContext _dbContext;

        public ExamToQuestionRepository(ZxZOnlineExamDbContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

        //所有的查询都要加上级联查询
        public override async Task<ExamToQuestion> GetSingleAsync(Guid id)
        {
            return await _dbContext.ExamToQuestions.Include(e => e.Exam).Include(e => e.Question).Where(e=>e.Id==id).FirstOrDefaultAsync();
        }

        public override async Task<IEnumerable<ExamToQuestion>> GetAsync()
        {
            return await _dbContext.ExamToQuestions.Include(e => e.Exam).Include(e => e.Question).ToListAsync();
        }
        public override async Task<IEnumerable<ExamToQuestion>> GetAsync(bool asc)
        {
            var result = _dbContext.ExamToQuestions.Include(e => e.Exam).Include(e => e.Question).AsQueryable();
            if (asc)
            {
                result = result.OrderBy(t => t.CreateTime);
            }
            else
            {
                result = result.OrderByDescending(t => t.CreateTime);
            }
            return await result.ToListAsync();
        }
        public override async Task<IEnumerable<ExamToQuestion>> GetAsync(int pageIndex, int pageSize = 10)
        {
            var result = _dbContext.ExamToQuestions.Include(e => e.Exam).Include(e => e.Question).AsQueryable();
            return await result.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }
        public override async Task<IEnumerable<ExamToQuestion>> GetAsync(Expression<Func<ExamToQuestion, bool>> expression)
        {
            var result = _dbContext.ExamToQuestions.Include(e => e.Exam).Include(e => e.Question).AsQueryable();
            result = result.Where(expression);
            return await result.ToListAsync();
        }
        public override async Task<IEnumerable<ExamToQuestion>> GetAsync(Expression<Func<ExamToQuestion, bool>> expression, bool asc)
        {
            var result = _dbContext.ExamToQuestions.Include(e => e.Exam).Include(e => e.Question).AsQueryable();
            if (asc)
            {
                result = result.OrderBy(t => t.CreateTime);
            }
            else
            {
                result = result.OrderByDescending(t => t.CreateTime);
            }
            result = result.Where(expression);
            return await result.ToListAsync();
        }
        public override async Task<IEnumerable<ExamToQuestion>> GetAsync(Expression<Func<ExamToQuestion, bool>> expression, bool asc, int pageIndex, int pageSize = 10)
        {
            var result = _dbContext.ExamToQuestions.Include(e => e.Exam).Include(e => e.Question).AsQueryable();
            if (asc)
            {
                result = result.OrderBy(t => t.CreateTime);
            }
            else
            {
                result = result.OrderByDescending(t => t.CreateTime);
            }
            result = result.Where(expression);
            return await result.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        }
    }
}
