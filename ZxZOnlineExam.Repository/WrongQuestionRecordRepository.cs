﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Repository
{
    public class WrongQuestionRecordRepository : BaseEntityRepository<WrongQuestionRecord>, IWrongQuestionRecordRepository
    {
        public WrongQuestionRecordRepository(ZxZOnlineExamDbContext dbContext) : base(dbContext)
        {
        }
    }
}
