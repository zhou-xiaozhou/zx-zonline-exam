﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Repository
{
    public class ExamRepository : BaseEntityRepository<Exam>, IExamRepository
    {
        private readonly ZxZOnlineExamDbContext _dbContext;

        public ExamRepository(ZxZOnlineExamDbContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }
        public override async Task<Exam> GetSingleAsync(Guid id)
        {
            return await this._dbContext.Exams.Include(e=>e.ZxZUser).Where(e=>e.Id==id).FirstOrDefaultAsync();
        }

    }
}
