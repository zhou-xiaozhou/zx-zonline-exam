﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Repository
{
    public class SubjectRepository : BaseEntityRepository<Subject>, ISubjectRepository
    {
        private readonly ZxZOnlineExamDbContext _dbContext;

        public SubjectRepository(ZxZOnlineExamDbContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

        //此处先考虑暂时不用虚方法，其实要查询指定层级的Subject只需加一个限制条件即可，不用考虑这么复杂
        //public override async Task<Subject> GetSingleAsync(Guid id)
        //{
        //    return await _dbContext.Set<Subject>().Include(t => t.Id).FirstOrDefaultAsync();
        //}
    }
}
