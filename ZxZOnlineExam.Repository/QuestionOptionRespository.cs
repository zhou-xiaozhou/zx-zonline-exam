﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Repository
{
    public class QuestionOptionRespository : BaseEntityRepository<QuestionOption>, IQuestionOptionRespository
    {
        public QuestionOptionRespository(ZxZOnlineExamDbContext dbContext) : base(dbContext)
        {
        }
    }
}
