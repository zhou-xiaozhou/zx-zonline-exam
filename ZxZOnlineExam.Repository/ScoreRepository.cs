﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Repository
{
    public class ScoreRepository : BaseEntityRepository<Score>, IScoreRepository
    {
        public ScoreRepository(ZxZOnlineExamDbContext dbContext) : base(dbContext)
        {
        }
    }
}
