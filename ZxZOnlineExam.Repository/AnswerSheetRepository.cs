﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Repository
{
    public class AnswerSheetRepository : BaseEntityRepository<AnswerSheet>, IAnswerSheetRepository
    {
        public AnswerSheetRepository(ZxZOnlineExamDbContext dbContext) : base(dbContext)
        {
        }
    }
}
