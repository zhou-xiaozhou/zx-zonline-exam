﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.IService
{
    public interface IBaseEntityService<T> where T:BaseEntity,new()
    {
        Task<IEnumerable<T>> GetAsync();
        Task<IEnumerable<T>> GetAsync(bool asc);
        Task<IEnumerable<T>> GetAsync(int pageIndex, int pageSize = 10);
        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression);
        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression, bool asc);
        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression, bool asc, int pageIndex, int pageSize = 10);

        Task<T> GetSingleAsync(Guid id);

        Task<int> GetTotalAsync();

        Task<bool> IsExsitedAsync(Guid id);

        Task<bool> CreateAsync(T T);

        Task<bool> UpdateAsync(T T);

        Task<bool> FakeDeleteAsync(Guid id);

        Task<bool> RealDeleteAsync(Guid id);

        Task<bool> SaveChangesAsync();
    }
}
