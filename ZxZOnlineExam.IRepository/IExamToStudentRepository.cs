﻿using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.IRepository
{
    public interface IExamToStudentRepository:IBaseEntityRepository<ExamToStudent>
    {
    }
}
