﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZxZOnlineExam.Dtos.ZxZUser
{
    public class ZxZUserDtoForChangePassword
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
