﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZxZOnlineExam.Dtos.ZxZUser
{
    public class ZxZUserDtoForQuery
    {
        /// <summary>
        /// 登陆名
        /// </summary>
        public string Username { get; set; }


        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }
    }
}
