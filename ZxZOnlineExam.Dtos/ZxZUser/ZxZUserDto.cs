﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZxZOnlineExam.Dtos.ZxZUser
{
    public class ZxZUserDto
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string Nickname { get; set; }

        public string Jwt { get; set; }
    }
}
