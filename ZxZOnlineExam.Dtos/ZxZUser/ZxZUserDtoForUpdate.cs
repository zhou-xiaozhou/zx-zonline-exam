﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ZxZOnlineExam.Dtos.ZxZUser
{
    public class ZxZUserDtoForUpdate
    {
        /// <summary>
        /// 昵称
        /// </summary>
        [StringLength(20)]
        //[Column(TypeName = "nvarchar(20)")]
        public string Nickname { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        [StringLength(100)]
        //[Column(TypeName = "nvarchar(100)")]
        public string Address { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        [StringLength(200)]
        //[Column(TypeName = "nvarchar(200)")]
        public string Introduction { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        [StringLength(200)]
        //[Column(TypeName = "nvarchar(200)")]
        public string HeadPortrait { get; set; }

        public string OldRoleName { get; set; }

        public string NewRoleName { get; set; }

    }
}
