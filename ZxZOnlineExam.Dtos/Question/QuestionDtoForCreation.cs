﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ZxZOnlineExam.Dtos.Question
{
    public class QuestionDtoForCreation
    {
        public string Title { get; set; }

        //[ForeignKey(nameof(Subject))]
        public Guid SubjectId { get; set; }
        //public SubjectDto SubjectDto { get; set; }

        /// <summary>
        /// QuestionType 枚举
        /// </summary>
        public string QuestionType { get; set; }

        //[ForeignKey(nameof(QuestionOption))]
        //public Guid QuestionOptionId { get; set; }
        //public QuestionOptionDto QuestionOptionDto { get; set; }

        //在创建Question时单独创建一个对应的QuestionOption
        public string OptionA { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionB { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionC { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionD { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionE { get; set; }

        [Required]
        //[Column(TypeName = "nvarchar(4000)"), StringLength(100, ErrorMessage = "长度不能超过1000")]
        public string CorrectAnswer { get; set; }
    }
}
