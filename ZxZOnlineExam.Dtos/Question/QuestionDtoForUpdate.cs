﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ZxZOnlineExam.Dtos.Question
{
    public class QuestionDtoForUpdate
    {
        //public Guid Id { get; set; } = Guid.NewGuid();//此Id遵循Ef命名规范，在UpdateDatabase时会自动映射为主键

        //public DateTime CreateTime { get; set; } = DateTime.UtcNow;

        //public bool IsRemoved { get; set; } = false;

        //public DateTime? UpdateTime { get; set; }


        public string Title { get; set; }

        //[ForeignKey(nameof(Subject))]
        public Guid SubjectId { get; set; }
        //public SubjectDto SubjectDto { get; set; }

        /// <summary>
        /// QuestionType 枚举
        /// </summary>
        public string QuestionType { get; set; }

        //[ForeignKey(nameof(QuestionOption))]
        //public Guid QuestionOptionId { get; set; } 一个Question指定一个QuestionOption，故不用更改
        //public QuestionOptionDto QuestionOptionDto { get; set; }

        public string OptionA { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionB { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionC { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionD { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionE { get; set; }

        [Required]
        //[Column(TypeName = "nvarchar(4000)"), StringLength(100, ErrorMessage = "长度不能超过1000")]
        public string CorrectAnswer { get; set; }
    }
}
