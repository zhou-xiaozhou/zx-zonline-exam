﻿using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.Dtos.QuestionOption;
using ZxZOnlineExam.Dtos.Subject;

namespace ZxZOnlineExam.Dtos.Question
{
    public class QuestionDto
    {
        public Guid Id { get; set; } = Guid.NewGuid();//此Id遵循Ef命名规范，在UpdateDatabase时会自动映射为主键

        public DateTime CreateTime { get; set; } = DateTime.UtcNow;

        public bool IsRemoved { get; set; } = false;

        public DateTime? UpdateTime { get; set; }


        public string Title { get; set; }

        //[ForeignKey(nameof(Subject))]
        public Guid SubjectId { get; set; }
        public SubjectDto SubjectDto { get; set; }

        /// <summary>
        /// QuestionType 枚举
        /// </summary>
        public string QuestionType { get; set; }

        //[ForeignKey(nameof(QuestionOption))]
        public Guid QuestionOptionId { get; set; }
        public QuestionOptionDto QuestionOptionDto { get; set; }
    }
}
