﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ZxZOnlineExam.Dtos.QuestionOption
{
    public class QuestionOptionDto
    {
        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionA { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionB { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionC { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionD { get; set; }

        //[Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionE { get; set; }

        [Required]
        //[Column(TypeName = "nvarchar(4000)"), StringLength(100, ErrorMessage = "长度不能超过1000")]
        public string CorrectAnswer { get; set; }

        /// <summary>
        /// Question 与QuestionOption就该是一对一的关系，因为一个Question肯定只能对应一个CorrectAnswer
        /// </summary>
        //[NotMapped]
        //public Question Question { get; set; }
    }
}
