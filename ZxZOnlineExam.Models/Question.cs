﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZxZOnlineExam.Models
{
    public class Question:BaseEntity
    {
        public string Title { get; set; }

        [ForeignKey(nameof(Subject))]
        public Guid SubjectId { get; set; }
        public Subject Subject { get; set; }

        /// <summary>
        /// QuestionType 枚举
        /// </summary>
        public string QuestionType { get; set; }

        [ForeignKey(nameof(QuestionOption))]
        public Guid QuestionOptionId { get; set; }
        public QuestionOption QuestionOption { get; set; }
    }
}
