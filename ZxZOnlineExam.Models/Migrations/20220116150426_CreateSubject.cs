﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZxZOnlineExam.Models.Migrations
{
    public partial class CreateSubject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Subjects",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    IsRemoved = table.Column<bool>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Introduction = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    FatherSubjectId = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subjects", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "21FA433E-2D36-DF50-53BA-C23701C65089",
                column: "ConcurrencyStamp",
                value: "fdb46bc8-4afa-47e9-bc24-47bdfd31fd56");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "822bb235-faba-485f-816e-7da4bd47170a",
                column: "ConcurrencyStamp",
                value: "468f14f9-9a48-469d-b590-3a404cfc1bf4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "F44B30E7-1AAE-A4AD-55DA-24B24B0730AE",
                column: "ConcurrencyStamp",
                value: "1da2b743-dc14-4041-b64a-609155166669");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4eb5db6d-b327-4aed-9785-a7863fd7d5d5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "50dedeaa-8994-43b6-86a6-601ae8be08ce", "AQAAAAEAACcQAAAAEO91VdevQm4D12y2vlT9PG5svf/iyo0uaHQNer1c0CqybsEImX09Wiq3H12Cjj95Vg==", "25e39016-879c-4f0e-8929-ace986b505ba" });

            migrationBuilder.InsertData(
                table: "Subjects",
                columns: new[] { "Id", "CreateTime", "FatherSubjectId", "Introduction", "IsRemoved", "Name", "UpdateTime" },
                values: new object[,]
                {
                    { new Guid("f68d7bc9-acb0-e75a-c775-1a6026b980c2"), new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(66), "1", null, false, "Subject1", null },
                    { new Guid("87a0fe7d-b76d-e968-b84c-33120ac3055b"), new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1242), "1", null, false, "Subject2", null },
                    { new Guid("961fd805-3b78-0c7c-278a-06affd3c08a5"), new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1261), "1", null, false, "Subject3", null },
                    { new Guid("a309340b-1422-214c-2c78-b7ad4f2a997e"), new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1264), "1", null, false, "Subject4", null },
                    { new Guid("67930473-e7a8-b6ba-cadf-19d80b269d4b"), new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1270), "1", null, false, "Subject5", null },
                    { new Guid("261c71e4-2ac6-072f-6af3-a5470694e8e2"), new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1295), "f68d7bc9-acb0-e75a-c775-1a6026b980c2", null, false, "Subject11", null },
                    { new Guid("04010076-92d2-6bb3-96e8-91234a0494eb"), new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1308), "f68d7bc9-acb0-e75a-c775-1a6026b980c2", null, false, "Subject11", null },
                    { new Guid("2f0eed1a-fbaf-7461-bb0a-b494c73875cb"), new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1325), "f68d7bc9-acb0-e75a-c775-1a6026b980c2", null, false, "Subject11", null },
                    { new Guid("009d81f7-8005-a313-ded2-b3af8f08638c"), new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1331), "f68d7bc9-acb0-e75a-c775-1a6026b980c2", null, false, "Subject11", null },
                    { new Guid("1415b964-d5b7-d769-7862-c1b6964bedb4"), new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1338), "f68d7bc9-acb0-e75a-c775-1a6026b980c2", null, false, "Subject11", null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Subjects");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "21FA433E-2D36-DF50-53BA-C23701C65089",
                column: "ConcurrencyStamp",
                value: "a4915906-fc5e-408c-9192-7cc8201d0f5b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "822bb235-faba-485f-816e-7da4bd47170a",
                column: "ConcurrencyStamp",
                value: "9e1b2141-f31c-420c-b3ca-2af71c25c306");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "F44B30E7-1AAE-A4AD-55DA-24B24B0730AE",
                column: "ConcurrencyStamp",
                value: "74bb51d3-0ddf-42cc-8486-cc00226b31e6");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4eb5db6d-b327-4aed-9785-a7863fd7d5d5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "46bb699c-d547-4576-a00e-54d679b5736f", "AQAAAAEAACcQAAAAEAilGnNF/gEf2YlubDRHCD8ezLMt+RapaoJkkwkG1/ZSbyHQDe516JCOJt3lqKBkyA==", "0bf461b6-4afe-42f3-b5d4-ad6a060a3728" });
        }
    }
}
