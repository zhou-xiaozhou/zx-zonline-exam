﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZxZOnlineExam.Models.Migrations
{
    public partial class CreateQuestionAndQuestionOption : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuestionOptions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    IsRemoved = table.Column<bool>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    OptionA = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    OptionB = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    OptionC = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    OptionD = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    OptionE = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CorrectAnswer = table.Column<string>(type: "nvarchar(4000)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionOptions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    IsRemoved = table.Column<bool>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    SubjectId = table.Column<Guid>(nullable: false),
                    QuestionType = table.Column<string>(nullable: true),
                    QuestionOptionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Questions_QuestionOptions_QuestionOptionId",
                        column: x => x.QuestionOptionId,
                        principalTable: "QuestionOptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Questions_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "21FA433E-2D36-DF50-53BA-C23701C65089",
                column: "ConcurrencyStamp",
                value: "ac45920f-f10e-4b02-8ce3-6df795c30e3e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "822bb235-faba-485f-816e-7da4bd47170a",
                column: "ConcurrencyStamp",
                value: "042d9c43-ecbf-45f8-afaa-9248b1777823");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "F44B30E7-1AAE-A4AD-55DA-24B24B0730AE",
                column: "ConcurrencyStamp",
                value: "4846c0c2-ce5d-465d-a050-141becefdf4c");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4eb5db6d-b327-4aed-9785-a7863fd7d5d5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "96355c06-cd77-43e4-b710-0052fd325b01", "AQAAAAEAACcQAAAAEGrCUqMA41Ix5hEim5X7yHSX8hADuENIJZOHxZTXnplyvu+M1hZ4b6YP8htyBG8k+A==", "86c26761-78ef-4c20-9802-d6fd1c29404f" });

            migrationBuilder.InsertData(
                table: "QuestionOptions",
                columns: new[] { "Id", "CorrectAnswer", "CreateTime", "IsRemoved", "OptionA", "OptionB", "OptionC", "OptionD", "OptionE", "UpdateTime" },
                values: new object[] { new Guid("2124e117-dc61-dd4c-39a7-00756e03d3d9"), "A、测试选项A", new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(5440), false, "A、测试选项A", "B、测试选项B", "C、测试选项C", "D、测试选项D", null, null });

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("009d81f7-8005-a313-ded2-b3af8f08638c"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3228));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("04010076-92d2-6bb3-96e8-91234a0494eb"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3216));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("1415b964-d5b7-d769-7862-c1b6964bedb4"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3235));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("261c71e4-2ac6-072f-6af3-a5470694e8e2"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3202));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("2f0eed1a-fbaf-7461-bb0a-b494c73875cb"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3222));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("67930473-e7a8-b6ba-cadf-19d80b269d4b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3176));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("87a0fe7d-b76d-e968-b84c-33120ac3055b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3132));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("961fd805-3b78-0c7c-278a-06affd3c08a5"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3152));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("a309340b-1422-214c-2c78-b7ad4f2a997e"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3155));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("f68d7bc9-acb0-e75a-c775-1a6026b980c2"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(1764));

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "CreateTime", "IsRemoved", "QuestionOptionId", "QuestionType", "SubjectId", "Title", "UpdateTime" },
                values: new object[,]
                {
                    { new Guid("522d2e9a-577a-2036-ee9f-f48070498e4b"), new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(1394), false, new Guid("2124e117-dc61-dd4c-39a7-00756e03d3d9"), "SingleChoice", new Guid("f68d7bc9-acb0-e75a-c775-1a6026b980c2"), "测试添加的第一条题目", null },
                    { new Guid("24f7b42b-9dbf-db43-4914-3aad12a3710b"), new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3747), false, new Guid("2124e117-dc61-dd4c-39a7-00756e03d3d9"), "SingleChoice", new Guid("f68d7bc9-acb0-e75a-c775-1a6026b980c2"), "测试添加的第2条题目", null },
                    { new Guid("e5b98011-453d-2972-4ee2-a94d600b4f66"), new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3825), false, new Guid("2124e117-dc61-dd4c-39a7-00756e03d3d9"), "SingleChoice", new Guid("f68d7bc9-acb0-e75a-c775-1a6026b980c2"), "测试添加的第3条题目", null },
                    { new Guid("e5525901-f2d8-fe36-f70d-d234459ed991"), new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3831), false, new Guid("2124e117-dc61-dd4c-39a7-00756e03d3d9"), "SingleChoice", new Guid("f68d7bc9-acb0-e75a-c775-1a6026b980c2"), "测试添加的第4条题目", null },
                    { new Guid("4bf1012f-c8fc-a0cc-c828-ff8df35877d6"), new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3836), false, new Guid("2124e117-dc61-dd4c-39a7-00756e03d3d9"), "SingleChoice", new Guid("f68d7bc9-acb0-e75a-c775-1a6026b980c2"), "测试添加的第5条题目", null },
                    { new Guid("22c7a368-298d-109f-b19c-25523d558033"), new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3841), false, new Guid("2124e117-dc61-dd4c-39a7-00756e03d3d9"), "SingleChoice", new Guid("87a0fe7d-b76d-e968-b84c-33120ac3055b"), "测试添加的第6条题目", null },
                    { new Guid("44aa65a9-6d5b-8b05-b79e-86be8d65fce9"), new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3845), false, new Guid("2124e117-dc61-dd4c-39a7-00756e03d3d9"), "SingleChoice", new Guid("87a0fe7d-b76d-e968-b84c-33120ac3055b"), "测试添加的第7条题目", null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Questions_QuestionOptionId",
                table: "Questions",
                column: "QuestionOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_SubjectId",
                table: "Questions",
                column: "SubjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "QuestionOptions");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "21FA433E-2D36-DF50-53BA-C23701C65089",
                column: "ConcurrencyStamp",
                value: "fdb46bc8-4afa-47e9-bc24-47bdfd31fd56");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "822bb235-faba-485f-816e-7da4bd47170a",
                column: "ConcurrencyStamp",
                value: "468f14f9-9a48-469d-b590-3a404cfc1bf4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "F44B30E7-1AAE-A4AD-55DA-24B24B0730AE",
                column: "ConcurrencyStamp",
                value: "1da2b743-dc14-4041-b64a-609155166669");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4eb5db6d-b327-4aed-9785-a7863fd7d5d5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "50dedeaa-8994-43b6-86a6-601ae8be08ce", "AQAAAAEAACcQAAAAEO91VdevQm4D12y2vlT9PG5svf/iyo0uaHQNer1c0CqybsEImX09Wiq3H12Cjj95Vg==", "25e39016-879c-4f0e-8929-ace986b505ba" });

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("009d81f7-8005-a313-ded2-b3af8f08638c"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1331));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("04010076-92d2-6bb3-96e8-91234a0494eb"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1308));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("1415b964-d5b7-d769-7862-c1b6964bedb4"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1338));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("261c71e4-2ac6-072f-6af3-a5470694e8e2"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1295));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("2f0eed1a-fbaf-7461-bb0a-b494c73875cb"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1325));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("67930473-e7a8-b6ba-cadf-19d80b269d4b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1270));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("87a0fe7d-b76d-e968-b84c-33120ac3055b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1242));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("961fd805-3b78-0c7c-278a-06affd3c08a5"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1261));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("a309340b-1422-214c-2c78-b7ad4f2a997e"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(1264));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("f68d7bc9-acb0-e75a-c775-1a6026b980c2"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 16, 15, 4, 25, 750, DateTimeKind.Utc).AddTicks(66));
        }
    }
}
