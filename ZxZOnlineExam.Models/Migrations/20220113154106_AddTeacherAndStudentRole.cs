﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ZxZOnlineExam.Models.Migrations
{
    public partial class AddTeacherAndStudentRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "822bb235-faba-485f-816e-7da4bd47170a",
                column: "ConcurrencyStamp",
                value: "9e1b2141-f31c-420c-b3ca-2af71c25c306");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Discriminator", "Name", "NormalizedName", "Introduction" },
                values: new object[,]
                {
                    { "21FA433E-2D36-DF50-53BA-C23701C65089", "a4915906-fc5e-408c-9192-7cc8201d0f5b", "ZxZRole", "Teacher", "TEACHER", "教师简介" },
                    { "F44B30E7-1AAE-A4AD-55DA-24B24B0730AE", "74bb51d3-0ddf-42cc-8486-cc00226b31e6", "ZxZRole", "Student", "STUDENT", "学生角色简介" }
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4eb5db6d-b327-4aed-9785-a7863fd7d5d5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "46bb699c-d547-4576-a00e-54d679b5736f", "AQAAAAEAACcQAAAAEAilGnNF/gEf2YlubDRHCD8ezLMt+RapaoJkkwkG1/ZSbyHQDe516JCOJt3lqKBkyA==", "0bf461b6-4afe-42f3-b5d4-ad6a060a3728" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "21FA433E-2D36-DF50-53BA-C23701C65089");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "F44B30E7-1AAE-A4AD-55DA-24B24B0730AE");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "822bb235-faba-485f-816e-7da4bd47170a",
                column: "ConcurrencyStamp",
                value: "449519ac-9139-41c9-8d18-0d32cdaf4728");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4eb5db6d-b327-4aed-9785-a7863fd7d5d5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "1c8ad1f4-522e-4af9-baa3-886095f0e525", "AQAAAAEAACcQAAAAEB03xCBn/Q8VFxtjKR2SodBVeY4LtMeG/7w6SODR0eLFNHNsaSWyE1egAQi4i2/Npg==", "515fa0a6-3482-4eb4-9fd2-67718f1fe74a" });
        }
    }
}
