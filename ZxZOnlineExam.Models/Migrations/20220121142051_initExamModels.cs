﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZxZOnlineExam.Models.Migrations
{
    public partial class initExamModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Exams",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    IsRemoved = table.Column<bool>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "nvarchar(20)", nullable: false),
                    SubjectId = table.Column<Guid>(nullable: false),
                    ZxzUserId = table.Column<string>(nullable: true),
                    ExamTime = table.Column<int>(type: "int", nullable: false),
                    DeadLineTime = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exams", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Exams_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Exams_AspNetUsers_ZxzUserId",
                        column: x => x.ZxzUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WrongQuestionRecords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    IsRemoved = table.Column<bool>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    ZxZUserId = table.Column<string>(nullable: true),
                    QuestionId = table.Column<Guid>(nullable: false),
                    WrongAnswer = table.Column<string>(type: "nvarchar(4000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WrongQuestionRecords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WrongQuestionRecords_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WrongQuestionRecords_AspNetUsers_ZxZUserId",
                        column: x => x.ZxZUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExamToQuestions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    IsRemoved = table.Column<bool>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    ExamId = table.Column<Guid>(nullable: false),
                    QuestionId = table.Column<Guid>(nullable: false),
                    ScoreValue = table.Column<decimal>(type: "decimal", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamToQuestions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExamToQuestions_Exams_ExamId",
                        column: x => x.ExamId,
                        principalTable: "Exams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExamToQuestions_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExamToStudents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    IsRemoved = table.Column<bool>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    ExamId = table.Column<Guid>(nullable: false),
                    ZxZUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamToStudents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExamToStudents_Exams_ExamId",
                        column: x => x.ExamId,
                        principalTable: "Exams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExamToStudents_AspNetUsers_ZxZUserId",
                        column: x => x.ZxZUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Scores",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    IsRemoved = table.Column<bool>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    ZxZUserId = table.Column<string>(nullable: true),
                    ExamId = table.Column<Guid>(nullable: false),
                    FinallyScore = table.Column<decimal>(type: "decimal(5,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Scores", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Scores_Exams_ExamId",
                        column: x => x.ExamId,
                        principalTable: "Exams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Scores_AspNetUsers_ZxZUserId",
                        column: x => x.ZxZUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AnswerSheets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    IsRemoved = table.Column<bool>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    ExamToQuestionId = table.Column<Guid>(nullable: false),
                    ZxZUserId = table.Column<string>(nullable: true),
                    Answer = table.Column<string>(type: "nvarchar(4000)", nullable: true),
                    IsCorrect = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnswerSheets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnswerSheets_ExamToQuestions_ExamToQuestionId",
                        column: x => x.ExamToQuestionId,
                        principalTable: "ExamToQuestions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AnswerSheets_AspNetUsers_ZxZUserId",
                        column: x => x.ZxZUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "21FA433E-2D36-DF50-53BA-C23701C65089",
                column: "ConcurrencyStamp",
                value: "4a00a783-55ea-4966-b431-ca7dd0736ca0");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "822bb235-faba-485f-816e-7da4bd47170a",
                column: "ConcurrencyStamp",
                value: "0fce1abe-5358-4b40-8f7a-32a8dab6d2bc");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "F44B30E7-1AAE-A4AD-55DA-24B24B0730AE",
                column: "ConcurrencyStamp",
                value: "c92f0db4-361d-4171-8d76-89749c452d0d");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4eb5db6d-b327-4aed-9785-a7863fd7d5d5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8440132c-d3f3-4655-b342-5e5d05c4ffdf", "AQAAAAEAACcQAAAAEEf8DqiB4uiS/8+3JxP+clDFM4LjNHWMki0yXyIrUxULiq1zhQK3Vd/YTMTg/8CWog==", "c12cfa0a-5d38-419e-88d0-1a858db3c3ea" });

            migrationBuilder.UpdateData(
                table: "QuestionOptions",
                keyColumn: "Id",
                keyValue: new Guid("2124e117-dc61-dd4c-39a7-00756e03d3d9"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(7313));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("22c7a368-298d-109f-b19c-25523d558033"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 814, DateTimeKind.Utc).AddTicks(3090));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("24f7b42b-9dbf-db43-4914-3aad12a3710b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 814, DateTimeKind.Utc).AddTicks(3001));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("44aa65a9-6d5b-8b05-b79e-86be8d65fce9"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 814, DateTimeKind.Utc).AddTicks(3095));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("4bf1012f-c8fc-a0cc-c828-ff8df35877d6"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 814, DateTimeKind.Utc).AddTicks(3086));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("522d2e9a-577a-2036-ee9f-f48070498e4b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 814, DateTimeKind.Utc).AddTicks(672));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("e5525901-f2d8-fe36-f70d-d234459ed991"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 814, DateTimeKind.Utc).AddTicks(3080));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("e5b98011-453d-2972-4ee2-a94d600b4f66"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 814, DateTimeKind.Utc).AddTicks(3072));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("009d81f7-8005-a313-ded2-b3af8f08638c"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(4905));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("04010076-92d2-6bb3-96e8-91234a0494eb"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(4886));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("1415b964-d5b7-d769-7862-c1b6964bedb4"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(4912));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("261c71e4-2ac6-072f-6af3-a5470694e8e2"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(4871));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("2f0eed1a-fbaf-7461-bb0a-b494c73875cb"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(4893));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("67930473-e7a8-b6ba-cadf-19d80b269d4b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(4852));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("87a0fe7d-b76d-e968-b84c-33120ac3055b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(4823));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("961fd805-3b78-0c7c-278a-06affd3c08a5"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(4846));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("a309340b-1422-214c-2c78-b7ad4f2a997e"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(4849));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("f68d7bc9-acb0-e75a-c775-1a6026b980c2"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 21, 14, 20, 50, 813, DateTimeKind.Utc).AddTicks(3646));

            migrationBuilder.CreateIndex(
                name: "IX_AnswerSheets_ExamToQuestionId",
                table: "AnswerSheets",
                column: "ExamToQuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_AnswerSheets_ZxZUserId",
                table: "AnswerSheets",
                column: "ZxZUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Exams_SubjectId",
                table: "Exams",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Exams_ZxzUserId",
                table: "Exams",
                column: "ZxzUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamToQuestions_ExamId",
                table: "ExamToQuestions",
                column: "ExamId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamToQuestions_QuestionId",
                table: "ExamToQuestions",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamToStudents_ExamId",
                table: "ExamToStudents",
                column: "ExamId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamToStudents_ZxZUserId",
                table: "ExamToStudents",
                column: "ZxZUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Scores_ExamId",
                table: "Scores",
                column: "ExamId");

            migrationBuilder.CreateIndex(
                name: "IX_Scores_ZxZUserId",
                table: "Scores",
                column: "ZxZUserId");

            migrationBuilder.CreateIndex(
                name: "IX_WrongQuestionRecords_QuestionId",
                table: "WrongQuestionRecords",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_WrongQuestionRecords_ZxZUserId",
                table: "WrongQuestionRecords",
                column: "ZxZUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnswerSheets");

            migrationBuilder.DropTable(
                name: "ExamToStudents");

            migrationBuilder.DropTable(
                name: "Scores");

            migrationBuilder.DropTable(
                name: "WrongQuestionRecords");

            migrationBuilder.DropTable(
                name: "ExamToQuestions");

            migrationBuilder.DropTable(
                name: "Exams");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "21FA433E-2D36-DF50-53BA-C23701C65089",
                column: "ConcurrencyStamp",
                value: "ac45920f-f10e-4b02-8ce3-6df795c30e3e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "822bb235-faba-485f-816e-7da4bd47170a",
                column: "ConcurrencyStamp",
                value: "042d9c43-ecbf-45f8-afaa-9248b1777823");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "F44B30E7-1AAE-A4AD-55DA-24B24B0730AE",
                column: "ConcurrencyStamp",
                value: "4846c0c2-ce5d-465d-a050-141becefdf4c");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4eb5db6d-b327-4aed-9785-a7863fd7d5d5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "96355c06-cd77-43e4-b710-0052fd325b01", "AQAAAAEAACcQAAAAEGrCUqMA41Ix5hEim5X7yHSX8hADuENIJZOHxZTXnplyvu+M1hZ4b6YP8htyBG8k+A==", "86c26761-78ef-4c20-9802-d6fd1c29404f" });

            migrationBuilder.UpdateData(
                table: "QuestionOptions",
                keyColumn: "Id",
                keyValue: new Guid("2124e117-dc61-dd4c-39a7-00756e03d3d9"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(5440));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("22c7a368-298d-109f-b19c-25523d558033"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3841));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("24f7b42b-9dbf-db43-4914-3aad12a3710b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3747));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("44aa65a9-6d5b-8b05-b79e-86be8d65fce9"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3845));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("4bf1012f-c8fc-a0cc-c828-ff8df35877d6"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3836));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("522d2e9a-577a-2036-ee9f-f48070498e4b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(1394));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("e5525901-f2d8-fe36-f70d-d234459ed991"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3831));

            migrationBuilder.UpdateData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: new Guid("e5b98011-453d-2972-4ee2-a94d600b4f66"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 600, DateTimeKind.Utc).AddTicks(3825));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("009d81f7-8005-a313-ded2-b3af8f08638c"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3228));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("04010076-92d2-6bb3-96e8-91234a0494eb"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3216));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("1415b964-d5b7-d769-7862-c1b6964bedb4"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3235));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("261c71e4-2ac6-072f-6af3-a5470694e8e2"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3202));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("2f0eed1a-fbaf-7461-bb0a-b494c73875cb"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3222));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("67930473-e7a8-b6ba-cadf-19d80b269d4b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3176));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("87a0fe7d-b76d-e968-b84c-33120ac3055b"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3132));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("961fd805-3b78-0c7c-278a-06affd3c08a5"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3152));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("a309340b-1422-214c-2c78-b7ad4f2a997e"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(3155));

            migrationBuilder.UpdateData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("f68d7bc9-acb0-e75a-c775-1a6026b980c2"),
                column: "CreateTime",
                value: new DateTime(2022, 1, 17, 15, 18, 45, 599, DateTimeKind.Utc).AddTicks(1764));
        }
    }
}
