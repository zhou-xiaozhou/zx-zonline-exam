﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZxZOnlineExam.Models
{
    /// <summary>
    /// 答题卡实体类
    /// </summary>
    public class AnswerSheet:BaseEntity
    {
        /// <summary>
        /// ExamToQuestion 外键，用于确定指定Exam的Rxpg指定Question对象
        /// </summary>
        [ForeignKey(nameof(ExamToQuestion))]
        public Guid ExamToQuestionId { get; set; }
        public ExamToQuestion ExamToQuestion { get; set; }

        /// <summary>
        /// ZxZUser 外键，用于确定指定ZxZUser对象提交的Answer
        /// </summary>
        [ForeignKey(nameof(ZxZUser))]
        public string ZxZUserId { get; set; }
        public ZxZUser ZxZUser { get; set; }

        /// <summary>
        /// Answer
        /// </summary>
        [Column(TypeName ="nvarchar(4000)")]
        public string Answer { get; set; }

        /// <summary>
        /// IsCorrect判断当前作答是否正确
        /// </summary>
        [Column(TypeName ="bit")]
        public bool IsCorrect { get; set; }
    }
}
