﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZxZOnlineExam.Models
{
    /// <summary>
    /// 考试学生类，用于指定Exam与ZxzUser的多对多关系，确定哪些User可以参加Exam
    /// </summary>
    public class ExamToStudent:BaseEntity
    {
        /// <summary>
        /// Exam foreignkey
        /// </summary>
        [ForeignKey(nameof(Subject))]
        public Guid ExamId { get; set; }
        public Exam Exam { get; set; }

        /// <summary>
        /// ZxZUser foreignkey
        /// </summary>
        [ForeignKey(nameof(ZxZUser))]
        public string ZxZUserId { get; set; }
        public ZxZUser ZxZUser { get; set; }
    }
}
