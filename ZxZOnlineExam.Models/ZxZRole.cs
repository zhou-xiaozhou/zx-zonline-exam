﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZxZOnlineExam.Models
{
    public class ZxZRole:IdentityRole
    {
        [StringLength(200)]
        [Column(TypeName = "nvarchar(200)")]
        public string Introduction { get; set; }
    }
}
