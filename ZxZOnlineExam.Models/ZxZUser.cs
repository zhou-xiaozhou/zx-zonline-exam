﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZxZOnlineExam.Models
{
    public class ZxZUser:IdentityUser
    {
        /// <summary>
        /// 昵称
        /// </summary>
        [StringLength(20)]
        [Column(TypeName ="nvarchar(20)")]
        public string Nickname { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        [StringLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        public string Address { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        [StringLength(200)]
        [Column(TypeName = "nvarchar(200)")]
        public string Introduction { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        [StringLength(200)]
        [Column(TypeName = "nvarchar(200)")]
        public string HeadPortrait { get; set; }


        /// <summary>
        ///用户角色
        /// </summary>
        public virtual ICollection<IdentityUserRole<string>> UserRoles { get; set; }
        /// <summary>
        /// 用户权限声明
        /// </summary>
        public virtual ICollection<IdentityUserClaim<string>> Claims { get; set; }
        /// <summary>
        /// 用户第三方登录信息
        /// </summary>
        public virtual ICollection<IdentityUserLogin<string>> Logins { get; set; }
        /// <summary>
        /// 用户登录的session
        /// </summary>
        public virtual ICollection<IdentityUserToken<string>> Tokens { get; set; }

    }
}
