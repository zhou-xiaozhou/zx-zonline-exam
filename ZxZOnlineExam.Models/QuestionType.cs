﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZxZOnlineExam.Models
{
    /// <summary>
    /// QuestionType 枚举类
    /// </summary>
    public enum QuestionType
    {
        SingleChoice,//0
        MultipleChoice,//1
        FillInTheBlank,//2
        QuestionAndAnswer//3
    }
}
