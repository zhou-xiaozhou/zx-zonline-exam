﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZxZOnlineExam.Models
{
    /// <summary>
    /// 考试 问题类，用于确定Exam与Question之间多对多关系
    /// </summary>
    public class ExamToQuestion:BaseEntity
    {
        /// <summary>
        /// Exam foreignkey
        /// </summary>
        [ForeignKey(nameof(Subject))]
        public Guid ExamId { get; set; }
        public Exam Exam { get; set; }


        /// <summary>
        /// Question foreignkey
        /// </summary>
        [ForeignKey(nameof(Question))]
        public Guid QuestionId { get; set; }
        public Question Question { get; set; }

        /// <summary>
        /// Question分值
        /// </summary>
        [Column(TypeName ="decimal")]
        public double ScoreValue { get; set; } = 1;
    }
}
