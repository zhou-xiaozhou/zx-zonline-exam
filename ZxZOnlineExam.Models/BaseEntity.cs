﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZxZOnlineExam.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();//此Id遵循Ef命名规范，在UpdateDatabase时会自动映射为主键

        public DateTime CreateTime { get; set; } = DateTime.UtcNow;

        public bool IsRemoved { get; set; } = false;

        public DateTime? UpdateTime { get; set; }
    }
}
