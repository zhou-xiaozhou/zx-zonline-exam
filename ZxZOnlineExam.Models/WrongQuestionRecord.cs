﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZxZOnlineExam.Models
{
    /// <summary>
    /// 错题本
    /// </summary>
    public class WrongQuestionRecord:BaseEntity
    {
        /// <summary>
        /// ZxZUser 外键，用于确定指定ZxZUser对象的一对一关系
        /// </summary>
        [ForeignKey(nameof(ZxZUser))]
        public string ZxZUserId { get; set; }
        public ZxZUser ZxZUser { get; set; }

        /// <summary>
        /// Question foreignkey
        /// </summary>
        [ForeignKey(nameof(Question))]
        public Guid QuestionId { get; set; }
        public Question Question { get; set; }

        /// <summary>
        /// ZxZUser所作的错误答案
        /// </summary>
        [Column(TypeName ="nvarchar(4000)")]
        public string WrongAnswer { get; set; }
    }
}
