﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;//模式；概要；图解；略图
using System.Text;

namespace ZxZOnlineExam.Models
{
    /// <summary>
    /// 分数表，用于确定指定ZxZUser在指定Exam中的考试成绩 
    /// </summary>
    public class Score:BaseEntity
    {
        /// <summary>
        /// ZxZUser 外键，用于确定指定ZxZUser对象Score 
        /// </summary>
        [ForeignKey(nameof(ZxZUser))]
        public string ZxZUserId { get; set; }
        public ZxZUser ZxZUser { get; set; }

        /// <summary>
        /// Exam foreignkey
        /// </summary>
        [ForeignKey(nameof(Subject))]
        public Guid ExamId { get; set; }
        public Exam Exam { get; set; }

        /// <summary>
        /// 考试最终得分，没有批改默认为0分
        /// </summary>
        [Column(TypeName ="decimal(5,2)")] //decimal(10,5)表示共有10位数，其中整数5位，小数5位。
        public double FinallyScore { get; set; } = 0;
    }
}
