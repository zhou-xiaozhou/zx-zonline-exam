﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZxZOnlineExam.Models
{
    public class QuestionOption:BaseEntity
    {
        //varchar(4) 可以输入4个字母，也可以输入两个汉字

        //nvarchar(4) 可以输四个汉字，也可以输4个字母，但最多四个
        [Column(TypeName ="nvarchar(100)"),StringLength(100,ErrorMessage ="长度不能超过100")]
        public string OptionA { get; set; }

        [Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionB { get; set; }

        [Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionC { get; set; }

        [Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionD { get; set; }

        [Column(TypeName = "nvarchar(100)"), StringLength(100, ErrorMessage = "长度不能超过100")]
        public string OptionE { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(4000)"), StringLength(100, ErrorMessage = "长度不能超过4000")]
        public string CorrectAnswer { get; set; }

        /// <summary>
        /// Question 与QuestionOption就该是一对一的关系，因为一个Question肯定只能对应一个CorrectAnswer
        /// </summary>
        [NotMapped]
        public Question Question { get; set; }
    }
}
