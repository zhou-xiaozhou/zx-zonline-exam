﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZxZOnlineExam.Models
{
    public class ZxZOnlineExamDbContext: IdentityDbContext<ZxZUser>
    {
        public ZxZOnlineExamDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //初始化用户与角色的种子数据
            //1.更新用户与角色的外键
            modelBuilder.Entity<ZxZUser>(u => u.HasMany(x => x.UserRoles).WithOne().HasForeignKey(ur => ur.UserId).IsRequired());
            //2.添加管理员角色
            var adminRoleId = "822bb235-faba-485f-816e-7da4bd47170a";
            modelBuilder.Entity<ZxZRole>().HasData(
                new ZxZRole()
                {
                    Id = adminRoleId,
                    Name = "Admin",
                    NormalizedName = "Admin".ToUpper(),
                    Introduction="系统管理员简介"
                });
            //添加教师角色
            var teacherRoleId = "21FA433E-2D36-DF50-53BA-C23701C65089";
            modelBuilder.Entity<ZxZRole>().HasData(
                new ZxZRole()
                {
                    Id = teacherRoleId,
                    Name = "Teacher",
                    NormalizedName = "Teacher".ToUpper(),
                    Introduction = "教师简介"
                });

            //添加学生角色
            var studentRoleId = "F44B30E7-1AAE-A4AD-55DA-24B24B0730AE";
            modelBuilder.Entity<ZxZRole>().HasData(
                new ZxZRole()
                {
                    Id = studentRoleId,
                    Name = "Student",
                    NormalizedName = "Student".ToUpper(),
                    Introduction = "学生角色简介"
                });


            //3.添加用户
            var adminUserId = "4eb5db6d-b327-4aed-9785-a7863fd7d5d5";
            ZxZUser adminUser = new ZxZUser
            {
                Id = adminUserId,
                UserName = "zxz",
                NormalizedUserName = "zxz".ToUpper(),
                Email = "1107960094@qq.com",
                NormalizedEmail = "1107960094@qq.com".ToUpper(),
                TwoFactorEnabled = false,
                EmailConfirmed = true,
                PhoneNumber = "123456789",
                PhoneNumberConfirmed = false
            };
            var ph = new PasswordHasher<ZxZUser>();
            adminUser.PasswordHash = ph.HashPassword(adminUser, "zxz");
            modelBuilder.Entity<ZxZUser>().HasData(adminUser);
            //4.给用户加入管理员角色
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>()
                {
                    RoleId = adminRoleId,
                    UserId = adminUserId
                });

            #region 添加Subject数据
            Guid subject1Id = new Guid("F68D7BC9-ACB0-E75A-C775-1A6026B980C2");
            Guid subject2Id = new Guid("87A0FE7D-B76D-E968-B84C-33120AC3055B");
            Guid subject3Id = new Guid("961FD805-3B78-0C7C-278A-06AFFD3C08A5");
            Guid subject4Id = new Guid("A309340B-1422-214C-2C78-B7AD4F2A997E");
            Guid subject5Id = new Guid("67930473-E7A8-B6BA-CADF-19D80B269D4B");

            modelBuilder.Entity<Subject>().HasData(
                new List<Subject>()
                {
                    //第一分类层级
                    new Subject(){Id=subject1Id,Name="Subject1",CreateTime=DateTime.UtcNow,FatherSubjectId="1"},
                    new Subject(){Id=subject2Id,Name="Subject2",CreateTime=DateTime.UtcNow,FatherSubjectId="1"},
                    new Subject(){Id=subject3Id,Name="Subject3",CreateTime=DateTime.UtcNow,FatherSubjectId="1"},
                    new Subject(){Id=subject4Id,Name="Subject4",CreateTime=DateTime.UtcNow,FatherSubjectId="1"},
                    new Subject(){Id=subject5Id,Name="Subject5",CreateTime=DateTime.UtcNow,FatherSubjectId="1"},

                    //第二分类层级,默认全部放在Subject1下
                    new Subject(){Id=new Guid("261C71E4-2AC6-072F-6AF3-A5470694E8E2"),Name="Subject11",CreateTime=DateTime.UtcNow,FatherSubjectId=subject1Id.ToString()},
                    new Subject(){Id=new Guid("04010076-92D2-6BB3-96E8-91234A0494EB"),Name="Subject11",CreateTime=DateTime.UtcNow,FatherSubjectId=subject1Id.ToString()},
                    new Subject(){Id=new Guid("2F0EED1A-FBAF-7461-BB0A-B494C73875CB"),Name="Subject11",CreateTime=DateTime.UtcNow,FatherSubjectId=subject1Id.ToString()},
                    new Subject(){Id=new Guid("009D81F7-8005-A313-DED2-B3AF8F08638C"),Name="Subject11",CreateTime=DateTime.UtcNow,FatherSubjectId=subject1Id.ToString()},
                    new Subject(){Id=new Guid("1415B964-D5B7-D769-7862-C1B6964BEDB4"),Name="Subject11",CreateTime=DateTime.UtcNow,FatherSubjectId=subject1Id.ToString()}


                }) ;
            #endregion

            #region 添加QuestionOption、Question数据
            //1、添加Option数据
            var option1Id = new Guid("2124E117-DC61-DD4C-39A7-00756E03D3D9");
            modelBuilder.Entity<QuestionOption>().HasData(
                new QuestionOption
                {
                    Id=option1Id,OptionA="A、测试选项A",OptionB= "B、测试选项B",OptionC= "C、测试选项C",OptionD= "D、测试选项D",CorrectAnswer= "A、测试选项A"
                }
                );
            modelBuilder.Entity<Question>().HasData(
                new Question
                {
                    Id = new Guid("522D2E9A-577A-2036-EE9F-F48070498E4B"),
                    Title = "测试添加的第一条题目",
                    SubjectId = subject1Id,
                    QuestionType = QuestionType.SingleChoice.ToString(),
                    QuestionOptionId = option1Id
                },
                new Question
                {
                    Id = new Guid("24F7B42B-9DBF-DB43-4914-3AAD12A3710B"),
                    Title = "测试添加的第2条题目",
                    SubjectId = subject1Id,
                    QuestionType = QuestionType.SingleChoice.ToString(),
                    QuestionOptionId = option1Id
                }, new Question
                {
                    Id = new Guid("E5B98011-453D-2972-4EE2-A94D600B4F66"),
                    Title = "测试添加的第3条题目",
                    SubjectId = subject1Id,
                    QuestionType = QuestionType.SingleChoice.ToString(),
                    QuestionOptionId = option1Id
                }, new Question
                {
                    Id = new Guid("E5525901-F2D8-FE36-F70D-D234459ED991"),
                    Title = "测试添加的第4条题目",
                    SubjectId = subject1Id,
                    QuestionType = QuestionType.SingleChoice.ToString(),
                    QuestionOptionId = option1Id
                },
                new Question
                {
                    Id = new Guid("4BF1012F-C8FC-A0CC-C828-FF8DF35877D6"),
                    Title = "测试添加的第5条题目",
                    SubjectId = subject1Id,
                    QuestionType = QuestionType.SingleChoice.ToString(),
                    QuestionOptionId = option1Id
                }, new Question
                {
                    Id = new Guid("22C7A368-298D-109F-B19C-25523D558033"),
                    Title = "测试添加的第6条题目",
                    SubjectId = subject2Id,
                    QuestionType = QuestionType.SingleChoice.ToString(),
                    QuestionOptionId = option1Id
                }, new Question
                {
                    Id = new Guid("44AA65A9-6D5B-8B05-B79E-86BE8D65FCE9"),
                    Title = "测试添加的第7条题目",
                    SubjectId = subject2Id,
                    QuestionType = QuestionType.SingleChoice.ToString(),
                    QuestionOptionId = option1Id
                }
                );
            #endregion


            base.OnModelCreating(modelBuilder);
            
        } 

        public DbSet<ZxZRole> ZxZRoles { get; set; }

        public DbSet<ZxZUser> ZxZUsers { get; set; }

        public DbSet<Subject> Subjects { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<QuestionOption> QuestionOptions { get; set; }

        #region Exam
        public DbSet<Exam> Exams  { get; set; }
        public DbSet<ExamToStudent> ExamToStudents { get; set; }
        public DbSet<ExamToQuestion> ExamToQuestions { get; set; }
        public DbSet<AnswerSheet> AnswerSheets { get; set; }
        public DbSet<Score> Scores { get; set; }
        public DbSet<WrongQuestionRecord> WrongQuestionRecords { get; set; }
        #endregion
    }
}
