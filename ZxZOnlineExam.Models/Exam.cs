﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZxZOnlineExam.Models
{




    /// <summary>
    /// 考试对象类
    /// </summary>
    public class Exam:BaseEntity
    {

        /// <summary>
        /// 名称
        /// </summary>
        [Required]
        [Column(TypeName ="nvarchar(20)")]
        public string Name { get; set; }

        /// <summary>
        /// Exam Subject foreignkey
        /// </summary>
        [ForeignKey(nameof(Subject))]
        public Guid SubjectId { get; set; }
        public Subject Subject { get; set; }

        /// <summary>
        /// Exam创建者信息
        /// </summary>
        [ForeignKey(nameof(ZxZUser))]
        public string ZxzUserId { get; set; }
        public ZxZUser ZxZUser { get; set; }

        /// <summary>
        /// 考试时长，默认为40分钟
        /// </summary>
        [Required]
        [Column(TypeName = "int")]
        public int ExamTime { get; set; } = 40;

        
        /// <summary>
        /// 考试最晚完成时间
        /// </summary>
        [Required]
        [Column(TypeName = "datetime")]
        //datetime用于存储时间和日期数据，从1753年1月1日到9999年12月31日，默认值为1900-01-01 00：00：00，当插入数据或在其它地方使用时，需用单引号或双引号括起来。可以使用“/”、“-”和“.”作为分隔符。该类型数据占用8个字节的空间
        public DateTime DeadLineTime { get; set; }

    }
}
