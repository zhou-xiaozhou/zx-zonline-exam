﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ZxZOnlineExam.Models
{
    public class Subject:BaseEntity
    {
        [Required(ErrorMessage ="名称不能为空")]
        [Column(TypeName ="nvarchar(20)"),StringLength(20,ErrorMessage ="长度在2到20",MinimumLength =2)]
        public string Name { get; set; }

        [Column(TypeName = "nvarchar(200)"), StringLength(200, ErrorMessage = "长度不能超过到200")]
        public string Introduction { get; set; }

        /// <summary>
        /// 父级分类Id，如果Id为空时表示此分类为第一层级
        /// </summary>
        [Column(TypeName = "nvarchar(50)")]
        //[ForeignKey(nameof(Id))]
        public string FatherSubjectId { get; set; } = "1";

        //public ICollection<Subject> Subjects { get; set; }
    }
}
