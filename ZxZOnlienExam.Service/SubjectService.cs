﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.IService;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Service
{
    public class SubjectService : BaseEntityService<Subject>, ISubjectService
    {
        public SubjectService(ISubjectRepository subjectRepository) : base(subjectRepository)
        {
        }
    }
}
