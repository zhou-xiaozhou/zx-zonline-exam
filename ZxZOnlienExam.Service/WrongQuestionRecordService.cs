﻿using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.IService;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Service
{
    public class WrongQuestionRecordService : BaseEntityService<WrongQuestionRecord>, IWrongQuestionRecordService
    {
        public WrongQuestionRecordService(IWrongQuestionRecordRepository baseEntityRepository) : base(baseEntityRepository)
        {
        }
    }
}
