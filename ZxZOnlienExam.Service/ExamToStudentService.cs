﻿using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.IService;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Service
{
    public class ExamToStudentService : BaseEntityService<ExamToStudent>, IExamToStudentService
    {
        public ExamToStudentService(IExamToStudentRepository baseEntityRepository) : base(baseEntityRepository)
        {
        }
    }
}
