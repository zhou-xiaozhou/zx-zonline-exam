﻿using System;
using System.Collections.Generic;
using System.Text;

using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.IService;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Service
{
    public class QuestionService : BaseEntityService<Question>,IQuestionService
    {
        public QuestionService(IQuestionRepository questionRepository) : base(questionRepository)
        {
        }
    }
}
