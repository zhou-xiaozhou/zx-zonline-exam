﻿using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.IService;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Service
{
    public class ExamToQuestionService : BaseEntityService<ExamToQuestion>, IExamToQuestionService
    {
        public ExamToQuestionService(IExamToQuestionRepository baseEntityRepository) : base(baseEntityRepository)
        {
        }
    }
}
