﻿using System;
using System.Collections.Generic;
using System.Text;

using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.IService;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Service
{
    public class QuestionOptionService : BaseEntityService<QuestionOption>, IQuestionOptionService
    {
        public QuestionOptionService(IQuestionOptionRespository questionOptionRespository) : base(questionOptionRespository)
        {
        }
    }
}
