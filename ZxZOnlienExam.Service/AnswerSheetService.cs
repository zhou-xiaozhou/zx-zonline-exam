﻿using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.IService;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Service
{
    public class AnswerSheetService : BaseEntityService<AnswerSheet>, IAnswerSheetService
    {
        public AnswerSheetService(IAnswerSheetRepository baseEntityRepository) : base(baseEntityRepository)
        {
        }
    }
}
