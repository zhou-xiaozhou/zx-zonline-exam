﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.IService;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Service
{
    public class BaseEntityService<T> : IBaseEntityService<T> where T:BaseEntity,new()
    {
        //在构造函数中传入IBaseEntityRepository对象
        private readonly IBaseEntityRepository<T> _baseEntityRepository;

        public BaseEntityService(IBaseEntityRepository<T> baseEntityRepository)
        {
            _baseEntityRepository = baseEntityRepository;
        }

        public async Task<bool> CreateAsync(T t)
        {
            return await _baseEntityRepository.CreateAsync(t);
        }

        public async Task<bool> FakeDeleteAsync(Guid id)
        {
            return await _baseEntityRepository.FakeDeleteAsync(id);
        }

        public async Task<IEnumerable<T>> GetAsync()
        {
            return await _baseEntityRepository.GetAsync();
        }

        public virtual async Task<IEnumerable<T>> GetAsync(bool asc)
        {
            return await _baseEntityRepository.GetAsync(asc);
        }

        public async Task<IEnumerable<T>> GetAsync(int pageIndex, int pageSize = 10)
        {
            return await _baseEntityRepository.GetAsync(pageIndex, pageSize);
        }

        public async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression)
        {
            return await _baseEntityRepository.GetAsync(expression);
        }

        public async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression, bool asc)
        {
            return await GetAsync(expression, asc);
        }

        public async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression, bool asc, int pageIndex, int pageSize = 10)
        {
            return await _baseEntityRepository.GetAsync(expression, asc, pageIndex, pageSize);
        }

        public async Task<T> GetSingleAsync(Guid id)
        {
            return await _baseEntityRepository.GetSingleAsync(id);
        }

        public async Task<int> GetTotalAsync()
        {
            return await _baseEntityRepository.GetTotalAsync();
        }

        public async Task<bool> IsExsitedAsync(Guid id)
        {
            return await _baseEntityRepository.IsExsitedAsync(id);
        }

        public async Task<bool> RealDeleteAsync(Guid id)
        {
            return await _baseEntityRepository.RealDeleteAsync(id);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _baseEntityRepository.SaveChangesAsync();
        }

        public async Task<bool> UpdateAsync(T t)
        {
            return await _baseEntityRepository.UpdateAsync(t);
        }
    }
}
