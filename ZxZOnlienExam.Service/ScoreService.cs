﻿using System;
using System.Collections.Generic;
using System.Text;
using ZxZOnlineExam.IRepository;
using ZxZOnlineExam.IService;
using ZxZOnlineExam.Models;

namespace ZxZOnlineExam.Service
{
    public class ScoreService : BaseEntityService<Score>, IScoreService
    {
        public ScoreService(IScoreRepository baseEntityRepository) : base(baseEntityRepository)
        {
        }
    }
}
